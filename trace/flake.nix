{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/21.11";
    swf_to_oarsub.url = "git+https://gitlab.inria.fr/qguillot/swf-to-oarsub";
  };

  outputs = { self, nixpkgs, swf_to_oarsub }:
    let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    mypkgs = swf_to_oarsub.packages.${system};
  in
   {
        devShells.${system}.default = pkgs.mkShell {
          buildInputs = [ mypkgs.swf_to_oar ];
        };


  };
}
