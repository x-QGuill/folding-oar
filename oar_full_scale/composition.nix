{ pkgs, lib, modulesPath, setup, ... }:
{
  nodes = let
    commonConfig =
      import ./common/common_config.nix { inherit pkgs lib modulesPath; };
  in {
    oar-server = { pkgs, ... }: {
      imports = [ commonConfig ];
      environment.etc."oar/api-users" = {
        mode = "0644";
        text = ''
          user1:$apr1$yWaXLHPA$CeVYWXBqpPdN78e5FvbY3/
          user2:$apr1$qMikYseG$VL8nyeSSmxXNe3YDOiCwr1
        '';
      };
      services.oar.client.enable = true;
      services.oar.server.enable = true;
      services.oar.dbserver.enable = true;
      services.oar.web = {
        enable = true;
        extraConfig = ''
          #To support remote_ident custom header
          underscores_in_headers on;
          location ^~ /oarapi-unsecure/ {
            rewrite ^/oarapi-unsecure/?(.*)$ /$1 break;
            include ${pkgs.nginx}/conf/uwsgi_params;
            uwsgi_pass unix:/run/uwsgi/oarapi.sock;
          }
        '';
      };
    };
    node = { pkgs, ... }: {
      imports = [ commonConfig ];
      services.oar.node = {
        enable = true;
        register = { enable = true; nbResources = setup.params.nbResources; };
      };
    };
  };
  testScript = ''
    node.execute("systemctl restart oar-node-register")
  '';
}
