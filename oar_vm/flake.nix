{
  description = "nixos-compose - basic setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/21.11";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
    NUR.url = "github:nix-community/NUR";
    kapack.url = "github:oar-team/nur-kapack/expes_cigri_quentin";
  };

  outputs = { self, nixpkgs, nxc, NUR, kapack }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};

      nur = nxc.lib.nur {
        inherit nixpkgs system NUR;
        repoOverrides = { inherit kapack; };
      };
      extraConfigurations = [
        { nixpkgs.overlays = [ nur.overlay ]; }
        nur.repos.kapack.modules.oar
        nur.repos.kapack.modules.cigri
        nur.repos.kapack.modules.my-startup
      ];
    in {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system extraConfigurations;
        composition = ./composition.nix;
        setup = ./setup.toml;
      };

      defaultPackage.${system} =
        self.packages.${system}."composition::nixos-test";

      devShell.${system} = nxc.devShells.${system}.nxcShellFull;
    };
}
