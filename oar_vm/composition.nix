{ pkgs, lib, modulesPath, setup, ... }: {
  nodes = let
    myflake = builtins.getFlake "${setup.params.pathOARFullScale}";
    nxcflake = myflake.inputs.nxc;
  in {
    machine = { pkgs, ... }:

      {
        users.users = {
          me = {
            isNormalUser = true;
            extraGroups = [ "wheel" ];
            password = "nixos";
          };
        };
        environment.etc."compose-info.json".text = builtins.readFile
          (myflake.packages.x86_64-linux."composition::vm-ramdisk");

        environment.systemPackages = [
          nxcflake.packages.x86_64-linux.nixos-compose
          pkgs.qemu_kvm
          pkgs.vde2
          pkgs.tmux

        ];
      };
  };
  testScript = "";
}
