{ pkgs, modulesPath, lib, ... }:
let
 get_oar_db_dump = pkgs.writeScriptBin "dump_oar" ''
    sudo -u oar psql -c "copy (
      select job_id,job_name,submission_time,start_time,stop_time,resources.resource_id
        FROM jobs, assigned_resources, moldable_job_descriptions, resources
        WHERE jobs.assigned_moldable_job = moldable_job_descriptions.moldable_id
            AND resources.type = 'default'
            AND assigned_resources.moldable_job_id = moldable_job_descriptions.moldable_id
            AND resources.resource_id = assigned_resources.resource_id
      )  to STDOUT with csv"
  '';
in
{
  environment.etc."oar-dbpassword".text = ''
    # DataBase user name
    DB_BASE_LOGIN="oar"
      
    # DataBase user password
    DB_BASE_PASSWD="oar"
    # DataBase read only user name
    DB_BASE_LOGIN_RO="oar_ro"
    # DataBase read only user password
    DB_BASE_PASSWD_RO="oar_ro" 
  '';
  services.oar = {
    # oar db passwords
    database = {
      host = "oar-server";
      passwordFile = "/etc/oar-dbpassword";
    };
    server.host = "oar-server";
    privateKeyFile = "/root/.ssh/id_rsa";
    publicKeyFile = "/root/.ssh/id_rsa.pub";
  };

  networking.firewall.enable = false;
  networking.firewall.allowedTCPPorts = [ 80 ];

  services.openssh = {
    enable = true;
    ports = [ 22 ];
    permitRootLogin = "yes";
  };

  users.users = {
    user1 = { isNormalUser = true; };
    user2 = { isNormalUser = true; };
    oar = { isSystemUser = true; group = "oar"; };
  };

  users.groups.oar = { };
  environment.systemPackages = with pkgs; [
    socat
    wget
    openssh
    nano
    get_oar_db_dump
  ];
}
